package br.com.lead.collector.security;

import br.com.lead.collector.services.LoginUsuairoSerivice;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FiltroAutorizacao extends BasicAuthenticationFilter {

    private JWTUtil jwtUtil;
    private LoginUsuairoSerivice loginUsuairoSerivice;

    public FiltroAutorizacao(AuthenticationManager authenticationManager, JWTUtil jwtUtil,
                             LoginUsuairoSerivice loginUsuairoSerivice) {
        super(authenticationManager);
        this.jwtUtil = jwtUtil;
        this.loginUsuairoSerivice = loginUsuairoSerivice;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {

        String authorizationHeader = request.getHeader("Authorization");

        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")){
            String tokenLimpo = authorizationHeader.substring(7);
            UsernamePasswordAuthenticationToken autenticacao = getAutenticacao(request, tokenLimpo);

            if(autenticacao != null ){
                SecurityContextHolder.getContext().setAuthentication(autenticacao);
            }
        }
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAutenticacao(HttpServletRequest request,String token){
        if(jwtUtil.tokenValido(token)){
            String username = jwtUtil.getUsername(token);
            UserDetails usuario = loginUsuairoSerivice.loadUserByUsername(username);
            return new UsernamePasswordAuthenticationToken(usuario, null, usuario.getAuthorities());
        }
        return null;
    }
}
