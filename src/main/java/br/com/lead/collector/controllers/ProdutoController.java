package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.LeadService;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    public Produto registrarProduto(@RequestBody Produto produto){
        Produto produtoObjeto = produtoService.salvarProduto(produto);
        return produtoObjeto;
    }

    @GetMapping
    public Iterable<Produto> buscarTodosProdutos(){
        Iterable<Produto> produtos = produtoService.buscarTodosOsProdutos();
        return produtos;
    }
}
